//
//  ResultCell.swift
//  MatrixScanSample
//
//  Created by Luca Torella on 26.10.17.
//  Copyright © 2017 Scandit. All rights reserved.
//

import UIKit

final class ResultCell: UITableViewCell {
    @IBOutlet weak var codeValue: UILabel!
    @IBOutlet weak var symbology: UILabel!
    @IBOutlet weak var quantity: UILabel!
}
