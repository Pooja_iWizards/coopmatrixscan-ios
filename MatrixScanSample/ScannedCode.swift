//
//  ScannedCode.swift
//  MatrixScanSample
//
//  Created by Luca Torella on 26.10.17.
//  Copyright © 2017 Scandit. All rights reserved.
//

import Foundation

struct ScannedCode: Hashable, Equatable {
    let data: String
    let symbology: String

    var hashValue: Int {
        return data.hashValue
    }

    static func ==(lhs: ScannedCode, rhs: ScannedCode) -> Bool {
        return lhs.data == rhs.data && lhs.symbology == rhs.symbology
    }
}

