//
//  AppDelegate.swift
//  MatrixScanSample
//
//  Created by Luca Torella on 02.03.17.
//  Copyright © 2017 Scandit. All rights reserved.
//

import UIKit
import ScanditBarcodeScanner

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        SBSLicense.setAppKey(kScanditBarcodeScannerAppKey)
        return true
    }
}

