//
//  ResultViewController.swift
//  MatrixScanSample
//
//  Created by Luca Torella on 26.10.17.
//  Copyright © 2017 Scandit. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private struct Constants {
        static let cellReuseIdentifier = "Cell"
    }

    var result: [(code: ScannedCode, quantity: UInt)] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scanAgainButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return result.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellReuseIdentifier, for: indexPath) as! ResultCell
        cell.codeValue.text = result[indexPath.row].code.data
        cell.symbology.text = result[indexPath.row].code.symbology
        cell.quantity.text = String(describing: result[indexPath.row].quantity)
        return cell
    }

    @objc private func close() {
        _ = navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func scanAgain(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}
