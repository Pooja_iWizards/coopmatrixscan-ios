//
//  ViewController.swift
//  MatrixScanSample
//
//  Created by Luca Torella on 02.03.17.
//  Copyright © 2017 Scandit. All rights reserved.
//

import UIKit
import ScanditBarcodeScanner

final class ViewController: UIViewController {
    @IBOutlet weak var doneButton: UIButton!
    private var barcodePicker: SBSBarcodePicker?
    /// Map from tracked id to SBSTrackedCode
    fileprivate var trackedIdToCodeMap: [UInt: SBSTrackedCode] = [:]
    /// All scanned codes
    fileprivate var scannedCodes: Set<String> = []
    fileprivate var scannedIndex = Int()
    fileprivate var addedIndex = Int()
    fileprivate var scannedIndexArr = NSMutableArray()
    fileprivate var sortedArr = NSMutableArray()
    fileprivate var scannedBarcodes = NSMutableArray()
    fileprivate var addedIndexArr = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupPicker()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        trackedIdToCodeMap = [:]
        scannedCodes = []
        barcodePicker?.startScanning()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        barcodePicker?.pauseScanning()
    }

    private func setupPicker() {
        let settings = buildScanSettings()
        let picker = SBSBarcodePicker(settings: settings)
        self.barcodePicker = picker
        // Set the GUI style to matrixScan in order to highlight tracked codes.
        picker.overlayController.guiStyle = .matrixScan
        // Register a SBSScanDelegate delegate, in order to be notified about relevant events.
        // (e.g. a successfully scanned bar code).
        picker.scanDelegate = self
        // Register a SBSProcessFrameDelegate delegate to be able to reject tracked codes.
        picker.processFrameDelegate = self

        // Add the barcode picker as a child view controller.
        addChildViewController(picker)
        picker.view.frame = view.bounds
        picker.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.insertSubview(picker.view, belowSubview: doneButton)
        picker.didMove(toParentViewController: self)
    }

    private func buildScanSettings() -> SBSScanSettings {
        // The scanning behavior of the barcode picker is configured through scan
        // settings. We start with empty scan settings and enable a generous set
        // of 1D symbologies. MatrixScan is currently only supported for 1D
        // symbologies, QR and DataMatrix codes, enabling 2D symbologies will result in unexpected results.
        // In your own apps, only enable the symbologies you actually need.
        let settings = SBSScanSettings.default()
        // Enable MatrixScan and set the max number of barcodes that can be recognized per frame
        // to some reasonable number for your use case. The max number of codes per frame does not
        // limit the number of codes that can be tracked at the same time, it only limits the
        // number of codes that can be newly recognized per frame.
        settings.isMatrixScanEnabled = true
        settings.maxNumberOfCodesPerFrame = 10
        settings.highDensityModeEnabled = true
        // Enable symbologies.
        let symbologies: Set<SBSSymbology> = [.ean8, .ean13, .upc12, .itf, .upce, .codabar,  .code11, .code128, .code25, .code39, .code93]
//        let symbologies: Set<SBSSymbology> = [.ean8, .ean13, .upc12, .upce, .code128, .aztec]
        for symbology in symbologies {
            settings.setSymbology(symbology, enabled: true)
        }
        return settings
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let resultViewController = segue.destination as? ResultViewController else {
            return
        }
        var countedResult: [ScannedCode: UInt] = [:]
        for (_, code) in trackedIdToCodeMap {
            guard let codeValue = code.data else { continue }
            let scannedCode = ScannedCode(data: codeValue, symbology: code.symbologyName)
            if let count = countedResult[scannedCode] {
                countedResult[scannedCode] = count + 1
            } else {
                countedResult[scannedCode] = 1
            }
        }
        resultViewController.result = countedResult.map { (key: ScannedCode, value: UInt) -> (ScannedCode, UInt) in

            return (key, value)
        }
    }
}

extension ViewController: SBSProcessFrameDelegate {
    func barcodePicker(_ barcodePicker: SBSBarcodePicker, didProcessFrame frame: CMSampleBuffer, session: SBSScanSession) {
        // If you want to visualy reject a code you should use SBSScanSession's rejectTrackedCode.
        // For example, the following commented code will reject all EAN8 codes.
//        guard let trackedCodes = session.trackedCodes else { return }
//        for (_, code) in trackedCodes {
//            if code.symbology == .ean8 {
//                session.rejectTrackedCode(code)
//            }
//        }

        // If you want to implement your own visualization of the code MatrixScan,
        // you should update it in this callback.
//        let imageBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(frame)!
//        let ciimage : CIImage = CIImage(cvPixelBuffer: imageBuffer)
//        let image : UIImage = self.convert(cmage: ciimage)
//        print(frame)

    }
    
    // Convert CIImage to CGImage
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
}

extension ViewController: SBSScanDelegate {
    func barcodePicker(_ picker: SBSBarcodePicker, didScan session: SBSScanSession) {
        // This delegate method acts the same as when not in MatrixScan and can be used for the events such as
        // when a code is newly recognized. Rejecting tracked codes has to be done in barcodePicker(_:didProcessFrame:session:).
        guard let trackedCodes = session.trackedCodes else { return }
        DispatchQueue.main.async {
            for (id, code) in trackedCodes where code.isRecognized {
                guard let data = code.data else { continue }
                if !self.scannedCodes.contains(data) {
                    if !data.isContainsLetters{
                    self.scannedCodes.insert(data)
                    self.checkIfdataExists(data: data)
                    }
                }else{
                    self.scannedCodes.removeAll()
                    self.checkIfdataExists(data: data)
                }
                self.trackedIdToCodeMap[id.uintValue] = code
            }

            for i in 0..<self.sortedArr.count {
                if scannedArr[i] as? String != barCodeArr[i] {
                    
                    let alert = UIAlertController(title: "Alert", message: "Incorrect order", preferredStyle: .alert)
                    
                                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                                        print("You've pressed default")
                                        scannedArr.removeAllObjects()
                                        self.sortedArr.removeAllObjects()
                                        self.scannedIndexArr.removeAllObjects()
                                        self.addedIndexArr.removeAllObjects()

                                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }else{
//                    let alert = UIAlertController(title: "Alert", message: "Correct order", preferredStyle: .alert)
//                    
//                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
//                        print("You've pressed default")
//                    }))
//                    
//                    self.present(alert, animated: true, completion: nil)
                }
            }


        }
    }
    
    func checkIfdataExists(data:String){
        if !scannedArr.contains(data){
            if !data.isContainsLetters{
            scannedArr.add(data)
            for i in 0..<scannedArr.count {
                if scannedArr[i] as? String == data{
                    self.scannedIndex = i
                    scannedIndexArr.add(i)
//                    if scannedIndexArr.count > 1 {
//                    sortedArr = scannedIndexArr.sorted (by: {($0 as AnyObject).count < ($1 as AnyObject).count}) as! NSMutableArray
//                    print(sortedArr)
//                    }else{
                        sortedArr.add(i)
//                    }
                    break
                }
            }
            for i in 0..<barCodeArr.count {
                if barCodeArr[i] == data{
                    self.addedIndex = i
                    self.addedIndexArr.add(i)
                    break
                }
            }
            
//            if self.scannedIndex != self.addedIndex{
//                let alert = UIAlertController(title: "Alert", message: "Incorrect order", preferredStyle: .alert)
//
//                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
//                    print("You've pressed default")
//                }))
//
//                self.present(alert, animated: true, completion: nil)
//            }
            }
            
        }
    }
}

extension String{
    var isContainsLetters : Bool{
        let letters = CharacterSet.letters
        return self.rangeOfCharacter(from: letters) != nil
    }
}

//extension Collection where Element: StringProtocol {
//    public func localizedStandardSorted(_ result: ComparisonResult) -> [Element] {
//        return sorted { $0.localizedStandardCompare($1) == result }
//    }
//}

